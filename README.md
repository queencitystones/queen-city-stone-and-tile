Our boutique style showroom in the heart of thriving SouthEnd is charming and filled with tile, stone, and furniture from all over the world. As a client of Queen City Stone, you work directly with one of two owners of the business. We’re passionate about helping people conceptualize their project and take great pride in beautifying Charlotte one home at a time!

Address: 2503 Distribution St, Charlotte, NC 28203

Phone: 704-837-7869